<?php
require_once ("Connection/NourishDB.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Nourish {

    private $db_handle;

    function __construct() {
        $this->db_handle = new NourishDB();
    }

    function getAgents() {

        $sql = "SELECT agent.id as id , agent.agent_id as agentId, agent.agent_code_for_datatable as agentCodeForDatatable, u.created_at as created,  p.full_name as full_name, p.cellphone_for_mapping as mobile ,p.address as address, u.email as email, agent.agent_type as agentType , u.zilla_id as district, u.upozilla_id as upozila  FROM sales_agents as agent
        INNER JOIN user_users u ON agent.user_id = u.id
        INNER JOIN user_profiles p ON u.id = p.user_id
        WHERE u.enabled = 1 AND agent.deleted_at IS NULL
        ORDER BY p.full_name ASC";

        $result = $this->db_handle->runBaseQuery($sql);
        $data = array();

        foreach($result as $key => $row) {
            $data[$key]['id']                   = (int) $row['id'];
            $data[$key]['agentId']              = $row['agentId'];
            $data[$key]['agentCodeForDatatable']= $row['agentCodeForDatatable'];
            $data[$key]['fullName']             = $row['full_name'];
            $data[$key]['created']              = $row['created'];
            $data[$key]['agentType']            = $row['agentType'];
            $data[$key]['district']             = $row['district'];
            $data[$key]['upozila']              = $row['upozila'];
            $data[$key]['phone']                = $row['mobile'];
            $data[$key]['email']                = $row['email'];
            $data[$key]['address']              = $row['address'];
        }
        return $data;
    }

    function getLocations() {

        $sql = "SELECT e.id as id , e.name as name , e.parent_id as parent, e.level as level  FROM core_locations as e
        ORDER BY e.level ASC";

        $result = $this->db_handle->runBaseQuery($sql);
        $data = array();

        foreach($result as $key => $row) {
            $data[$key]['id']                = (int) $row['id'];
            $data[$key]['name']              = $row['name'];
            $data[$key]['parent']            = $row['parent'];
            $data[$key]['level']             = $row['level'];
        }
        return $data;
    }

    function  getOrders()
    {
        $firstMonth = date("Y-m-d 00:00:00", strtotime("first day of previous month"));
        $lastMonth = date("Y-m-t 23:59:59", strtotime("first day of previous month"));

        $sql = "SELECT e.id as id , e.total_amount as total , e.created_at as created, order_state as process  FROM sales_orders as e
        ORDER BY e.created DESC  LIMIT 30";
        $result = $this->db_handle->runBaseQuery($sql);
        $data = array();
        foreach($result as $key => $row) {
            $data[$key]['id']                = (int) $row['id'];
            $data[$key]['total']             = $row['total'];
            $data[$key]['process']           = $row['process'];
            $data[$key]['created']           = $row['created'];
        }

        return $data;

    }

    function  getOrderItems()
    {

        $firstMonth = date("Y-m-d 00:00:00", strtotime("first day of previous month"));
        $lastMonth = date("Y-m-t 23:59:59", strtotime("first day of previous month"));

        $sql = "SELECT agent_id AS agentId , s.location_id AS upozila, it.id AS id, it.item_types AS itemTypeName ,SUM(si.total_amount) AS amount ,SUM(si.quantity) AS quantity FROM 
`sales_order_items` AS si 
JOIN sales_orders AS s ON si.order_id = s.id
JOIN core_items AS i ON si.item_id = i.id
JOIN core_item_types AS it ON i.item_types = it.id
WHERE  s.created_at >= '{$firstMonth}' AND s.created_at <= '{$lastMonth}'
GROUP BY it.id ,  s.`agent_id`
ORDER BY s.`agent_id` ASC";
        $result = $this->db_handle->runBaseQuery($sql);
        $data = array();

        foreach($result as $key => $row) {

            $month = date('F',strtotime($firstMonth));
            $year = date('Y',strtotime($firstMonth));
            $created = date('Y-m-d 10:30:30',strtotime($firstMonth));

            $data[$key]['agentId']                = (int) $row['agentId'];
            $data[$key]['upozila']                = (int) $row['upozila'];
            $data[$key]['itemName']               = $row['itemTypeName'];
            $data[$key]['quantity']               = $row['quantity'];
            $data[$key]['amount']                 = $row['amount'];
            $data[$key]['created']                = $created;
            $data[$key]['month']                  = $month;
            $data[$key]['year']                   = $year;
        }

        return $data;

    }
}
?>