<?php
require_once ("Connection/TerminalbdDB.php");
class Terminalbd {

    private $db_handle;

    function __construct() {
        $this->db_handle = new TerminalbdDB();
    }

    function getStockItem() {

        $sql = "SELECT product.*
        FROM medicine_stock as product
       WHERE product.status =1
       GROUP BY product.id
       ORDER BY product.name ASC  LIMIT 30";
        $result = $this->db_handle->runBaseQuery($sql);
        $data = array();

        foreach($result as $key => $row) {

            $data[$key]['item_id']              = (int) $row['stockId'];
            $data[$key]['name']                 = $row['name'];
            $data[$key]['quantity']             = $row['remainingQuantity'];
            $data[$key]['salesPrice']           = $row['salesPrice'];
            $data[$key]['purchasePrice']        = $row['purchasePrice'];


        }
        return $data;
    }
}
?>