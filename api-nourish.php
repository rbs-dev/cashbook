<?php
require_once ("Connection/NourishDB.php");
require_once ("Api/Nourish.php");
$db_handle = new NourishDB();

if (! empty($_GET["action"])) {
    $action = $_GET["action"];
}

switch ($action) {
    case "agent":
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = new Nourish();
        $data = $terminal->getAgents();
        header('Access-Control-Allow-Origin: *');
        header("Content-type: application/json; charset=utf-8");
        echo  json_encode($data);
        break;
    case "location":
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = new Nourish();
        $data = $terminal->getLocations();
        header('Access-Control-Allow-Origin: *');
        header("Content-type: application/json; charset=utf-8");
        echo  json_encode($data);
        break;
    case "order":
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = new Nourish();
        $data = $terminal->getOrders();
        header('Access-Control-Allow-Origin: *');
        header("Content-type: application/json; charset=utf-8");
        echo  json_encode($data);
        break;
    case "order-item":
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = new Nourish();
        $data = $terminal->getOrderItems();
        header('Access-Control-Allow-Origin: *');
        header("Content-type: application/json; charset=utf-8");
        echo  json_encode($data);
        break;
    default:
        $student = new Student();
        $result = $student->getAllStudent();
        require_once "web/student.php";
        break;
}
?>