<!-- Header -->
<?php require_once ("layout/header.php"); ?>
<!-- End Header -->
<!-- Sidebar -->
<?php require_once ("layout/sidebar.php"); ?>
<!-- End Sidebar -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Responsive Table</div>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-sm" id="btnAddAction" href="index.php?action=student-add"><i class="pe-7s-plus"></i> New Student</a>
                                    <a class="btn btn-sm btn-primary btn-border" id="btnAddAction" href="index.php?action=student-add"><i class="pe-7s-light"></i> Student</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <form name="frmAdd" method="post" action="" id="frmAdd"
                                      onSubmit="return validate();">
                                    <div id="mail-status"></div>
                                    <div>
                                        <label style="padding-top: 20px;">Name</label> <span
                                                id="name-info" class="info"></span><br /> <input type="text"
                                                                                                 name="name" id="name" class="demoInputBox"
                                                                                                 value="<?php echo $result[0]["name"]; ?>">
                                    </div>
                                    <div>
                                        <label>Roll Number</label> <span id="roll-number-info"
                                                                         class="info"></span><br /> <input type="text"
                                                                                                           name="roll_number" id="roll_number" class="demoInputBox"
                                                                                                           value="<?php echo $result[0]["roll_number"]; ?>">
                                    </div>
                                    <div>
                                        <label>Date of Birth</label> <span id="dob-info" class="info"></span><br />
                                        <input type="text" name="dob" id="dob" class="demoInputBox"
                                               value="<?php echo $result[0]["dob"]; ?>">
                                    </div>
                                    <div>
                                        <label>Class</label> <span id="class-info" class="info"></span><br />
                                        <input type="text" name="class" id="class" class="demoInputBox"
                                               value="<?php echo $result[0]["class"]; ?>">
                                    </div>
                                    <div>
                                        <input type="submit" name="add" id="btnSubmit" value="Save" />
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<?php require_once ("layout/footer.php"); ?>
<!-- End Footer -->
