<!-- Header -->
<?php require_once ("layout/header.php"); ?>
<!-- End Header -->
<!-- Sidebar -->
<?php require_once ("layout/sidebar.php"); ?>
<!-- End Sidebar -->
<!-- Body -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Responsive Table</div>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-sm" id="btnAddAction" href="index.php?action=student-add"><i class="pe-7s-plus"></i> New Student</a>
                                    <a class="btn btn-sm btn-primary btn-border" id="btnAddAction" href="index.php?action=student-add"><i class="pe-7s-light"></i> Student</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-bordered table-head-bg-info">
                                    <thead>
                                    <tr>
                                        <th><strong>Name</strong></th>
                                        <th><strong>Roll Number</strong></th>
                                        <th><strong>Date of Birth</strong></th>
                                        <th><strong>Class</strong></th>
                                        <th><strong>Action</strong></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if (! empty($result)) {
                                        foreach ($result as $k => $v) {
                                            ?>
                                            <tr>
                                                <td><?php echo $result[$k]["name"]; ?></td>
                                                <td><?php echo $result[$k]["roll_number"]; ?></td>
                                                <td><?php echo $result[$k]["dob"]; ?></td>
                                                <td><?php echo $result[$k]["class"]; ?></td>
                                                <td><a class="btnEditAction"
                                                       href="index.php?action=student-edit&id=<?php echo $result[$k]["id"]; ?>">
                                                        <img src="web/image/icon-edit.png" />
                                                    </a>
                                                    <a class="btnDeleteAction"
                                                       href="index.php?action=student-delete&id=<?php echo $result[$k]["id"]; ?>">
                                                        <img src="web/image/icon-delete.png" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once ("layout/header.php"); ?>
<!-- End Body -->
<!-- Footer -->
<?php require_once ("layout/footer.php"); ?>
<!-- End Footer -->


