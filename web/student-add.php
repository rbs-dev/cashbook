<!-- Header -->
<?php require_once ("layout/header.php"); ?>
<!-- End Header -->
<!-- Sidebar -->
<?php require_once ("layout/sidebar.php"); ?>
<!-- End Sidebar -->
<!-- Body -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Responsive Table</div>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-sm btn-border"  id="btnAddAction" href="index.php?action=student-add"><i class="flaticon-plus"></i> New Student</a>
                                    <a class="btn btn-sm btn-primary " id="btnAddAction" href="index.php?action=student-add"><i class="la flaticon-list"></i> Student</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <form name="frmAdd" method="post" action="" id="frmAdd"
                                      onSubmit="return validate();">
                                    <div id="mail-status"></div>
                                    <div>
                                        <label style="padding-top: 20px;">Name</label> <span
                                                id="name-info" class="info"></span><br /> <input type="text"
                                                                                                 name="name" id="name" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Roll Number</label> <span id="roll-number-info"
                                                                         class="info"></span><br /> <input type="text"
                                                                                                           name="roll_number" id="roll_number" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Date of Birth</label> <span id="dob-info" class="info"></span><br />
                                        <input type="date" name="dob" id="dob" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Class</label> <span id="class-info" class="info"></span><br />
                                        <input type="text" name="class" id="class" class="demoInputBox">
                                    </div>
                                    <div>
                                        <input type="submit" name="add" id="btnSubmit" value="Add" />
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Base Form Control</div>
                        </div>
                        <div class="form-wrap">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="exampleInputuname_4" class="col-sm-3 control-label">Username*</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="exampleInputuname_4" placeholder="Username">
                                            <div class="input-group-addon"><i class="icon-user"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail_4" class="col-sm-3 control-label">Email*</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="email" class="form-control" id="exampleInputEmail_4" placeholder="Enter email">
                                            <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputweb_41" class="col-sm-3 control-label">Website</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="exampleInputweb_41" placeholder="Enter Website Name">
                                            <div class="input-group-addon"><i class="icon-globe"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd_5" class="col-sm-3 control-label">Password*</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="exampleInputpwd_5" placeholder="Enter pwd">
                                            <div class="input-group-addon"><i class="icon-lock"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd_6" class="col-sm-3 control-label">Re Password*</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="exampleInputpwd_6" placeholder="Re Enter pwd">
                                            <div class="input-group-addon"><i class="icon-lock"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox_34" type="checkbox">
                                            <label for="checkbox_34">Check me out !</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info ">Sign in</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">

                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Name</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control form-control-sm input-full" id="inlineinput" placeholder="Enter Input">
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Inline Input</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control form-control-sm input-full" id="inlineinput" placeholder="Enter Input">
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once ("layout/footer.php"); ?>
<!-- End Footer -->
