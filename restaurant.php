<?php
require_once ("class/DBController.php");
require_once ("class/Student.php");
require_once ("class/Attendance.php");

$db_handle = new DBController();

// $action = "";
if (! empty($_GET["action"])) {
    $action = $_GET["action"];
}
switch ($action) {

    case "product":
        $attendance = new Attendance();
        $result = $attendance->getAttendance();
        require_once "restaurant/product.php";
        break;

    case "table":
        $attendance = new Attendance();
        $result = $attendance->getAttendance();
        require_once "restaurant/table.php";
        break;

    case "waiter":
        $attendance = new Attendance();
        $result = $attendance->getAttendance();
        require_once "restaurant/waiter.php";
        break;

    case "setting":
        $attendance = new Attendance();
        $result = $attendance->getAttendance();
        require_once "restaurant/setting.php";
        break;

    default:
        $student = new Student();
        $result = $student->getAllStudent();
        require_once "restaurant/register.php";
        break;
}
?>