<!-- Footer -->
</div>
<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- jQuery UI -->
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<!-- Bootstrap Toggle -->
<script src="assets/js/bootstrap-toggle.min.js"></script>
<!-- jQuery Scrollbar -->
<script src="assets/js/jquery.scrollbar.min.js"></script>
<!--<script src="assets/js/bootstrapwizard.js"></script>-->
<script src="assets/js/jquery.validate.min.js"></script>
<!-- jQuery Musk -->
<script src="assets/plugins/jquery-musk/bootstrap-inputmask.min.js"></script>
<!-- Azzara JS -->
<script src="assets/js/ready.js"></script>
<script src="assets/js/nav.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#horizontalMenu").horizontalResponsiveMenu({
            resizeWidth: '768', // Set the same in Media query
            animationSpeed: 'fast', //slow, medium, fast
            accoridonExpAll: false //Expands all the accordion menu on click
        });
    });
</script>
<script>
    function validate() {
        var valid = true;
        $(".demoInputBox").css('background-color','');
        $(".info").html('');

        if(!$("#name").val()) {
            $("#name-info").html("(required)");
            $("#name").css('background-color','#FFFFDF');
            valid = false;
        }
        if(!$("#roll_number").val()) {
            $("#roll-number-info").html("(required)");
            $("#roll_number").css('background-color','#FFFFDF');
            valid = false;
        }
        if(!$("#dob").val()) {
            $("#dob-info").html("(required)");
            $("#dob").css('background-color','#FFFFDF');
            valid = false;
        }
        if(!$("#class").val()) {
            $("#class-info").html("(required)");
            $("#class").css('background-color','#FFFFDF');
            valid = false;
        }
        return valid;
    }
</script>
</body>
</html>
