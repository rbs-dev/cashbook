<!-- Header -->
<?php require_once ("layout/header.php"); ?>
<!-- End Header -->
<!-- Sidebar -->
<?php require_once ("layout/sidebar.php"); ?>
<!-- End Sidebar -->
<!-- Body -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Responsive Table</div>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-sm btn-border" id="btnAddAction" href=""><i class="flaticon-plus"></i> New Student</a>
                                    <a class="btn btn-sm btn-primary " id="btnAddAction" href=""><i class="la flaticon-list"></i> Student</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="wizard-container wizard-round col-md-11">
                                    <div class="wizard-header text-center">
                                        <h3 class="wizard-title"><b>Register</b> New Account</h3>
                                        <small>To build your own restaurant</small>
                                    </div>
                                    <form novalidate="novalidate">
                                        <div class="wizard-body">
                                            <div class="row">
                                                <ul class="wizard-menu nav nav-pills nav-primary">
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link" href="#about" data-toggle="tab" aria-expanded="true"><i class="fa fa-user mr-0"></i> Register</a>
                                                    </li>
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link active show" href="#account" data-toggle="tab"><i class="fa fa-file mr-2"></i> Product</a>
                                                    </li>
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link" href="#address" data-toggle="tab"><i class="fa fa-map-signs mr-2"></i> Table</a>
                                                    </li>
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link" href="#about" data-toggle="tab" aria-expanded="true"><i class="fa fa-user mr-0"></i> Waiter</a>
                                                    </li>
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link" href="#account" data-toggle="tab"><i class="fa fa-file mr-2"></i> Settings</a>
                                                    </li>
                                                    <li class="step" style="width: 33.3333%;">
                                                        <a class="nav-link" href="#address" data-toggle="tab"><i class="fa fa-map-signs mr-2"></i> POS</a>
                                                    </li>
                                                    <div class="moving-tab" style="width: 291.233px; transform: translate3d(0px, 0px, 0px); transition: all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1) 0s;"><i class="fa fa-user mr-0"></i> About</div><div class="moving-tab" style="width: 291.233px; transform: translate3d(0px, 0px, 0px); transition: all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1) 0s;"><i class="fa fa-user mr-0"></i> About</div></ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane" id="about">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4 class="info-text">Tell us who you are.</h4>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <div class="form-group form-group-default">
                                                                <label>Restaurant Name</label>
                                                                <input id="restaurant" name="restaurant"  type="text" class="form-control" placeholder="Restaurant name" required>
                                                            </div>
                                                            <div class="form-group form-group-default">
                                                                <label>Mobile No</label>
                                                                <input id="mobile"  name="mobile"  type="text" class="form-control mobile" placeholder="Enter mobile no" data-mask="99999-999999" required>
                                                            </div>

                                                            <div class="form-group form-group-default">
                                                                <label>Address</label>
                                                                <input id="address" name="address"  type="text" class="form-control" placeholder="Enter restaurant address" required>
                                                            </div>

                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group form-group-default">
                                                                <label>Owner Name</label>
                                                                <input id="ownerName" name="ownerName"  type="text" class="form-control" placeholder="Enter restaurant owner" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Picture :</label>
                                                                <div class="input-file input-file-image">
                                                                    <img class="img-upload-preview img-circle" width="100" height="100" src="assets/img/100x100" alt="preview">
                                                                    <input type="file" class="form-control form-control-file" id="uploadImg" name="uploadImg" accept="image/*" required="">
                                                                    <label for="uploadImg" class=" label-input-file btn btn-primary">Upload a Image</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane active" id="account">
                                                    <h4 class="info-text">Set up your account </h4>
                                                    <div class="row">
                                                        <div class="col-md-8 ml-auto mr-auto">
                                                            <div class="form-group">
                                                                <label>Email :</label>
                                                                <input type="email" name="email" class="form-control" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Password :</label>
                                                                <input type="password" name="password" class="form-control" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Confirm Password :</label>
                                                                <input type="password" name="confirmpassword" class="form-control" required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="address">
                                                    <h4 class="info-text">Tell us where you live.</h4>
                                                    <div class="row">
                                                        <div class="col-sm-8 ml-auto mr-auto">
                                                            <div class="form-group">
                                                                <label>Country :</label>

                                                                <select name="country" class="form-control" required="">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="id">Indonesia</option>
                                                                    <option value="my">Malaysia</option>
                                                                    <option value="th">Thailand</option>
                                                                    <option value="sg">Singapore</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Address</label>

                                                                <textarea name="address" rows="3" class="form-control" required=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="wizard-action">
                                            <div class="pull-left">
                                                <input type="button" class="btn btn-previous btn-fill btn-default disabled" name="previous" value="Previous">
                                            </div>
                                            <div class="pull-right">
                                                <input type="button" class="btn btn-next btn-danger" id="next" name="next" value="Next">
                                                <input type="button" class="btn btn-finish btn-danger" name="finish" value="Finish" style="display: none;">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .has-success label {
        color: #0442ba !important;
    }
    .has-success .form-control {
        border-color: #0442ba !important;
        color: #323436 !important;
    }
</style>
<!-- Footer -->
<?php require_once ("layout/footer.php"); ?>
<!-- End Footer -->
<script>
    var $validator = $('.wizard-container form').validate({
        validClass : "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
    });

    $(document).ready(function(){

        $("#next").click(function(){

            var fd = new FormData();
            var files = $('#uploadImg')[0].files[0];
            fd.append('file',files);
            $.ajax({
                url: 'ajax-form.php?action=register',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0){
                        $("#img").attr("src",response);
                        $(".preview img").show(); // Display image element
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        });
    });
</script>