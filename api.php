<?php
require_once ("Connection/TerminalbdDB.php");
require_once ("Api/Terminalbd.php");
$db_handle = new TerminalbdDB();

if (! empty($_GET["action"])) {
    $action = $_GET["action"];
}

switch ($action) {
    case "agent":
        $terminal = new Terminalbd();
        $stocks = $terminal->getStockItem();
        header('Access-Control-Allow-Origin: *');
        header("Content-type: application/json; charset=utf-8");
        echo  json_encode($stocks);
        break;
    default:
        $student = new Student();
        $result = $student->getAllStudent();
        require_once "web/student.php";
        break;
}
?>