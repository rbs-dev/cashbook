<!-- Header -->
<?php require_once ("layout/header.php"); ?>
<!-- End Header -->
<!-- Sidebar -->
<?php require_once ("layout/sidebar.php"); ?>
<!-- End Sidebar -->
<!-- Body -->
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Responsive Table</div>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-primary btn-sm btn-border" id="btnAddAction" href=""><i class="flaticon-plus"></i> New Student</a>
                                    <a class="btn btn-sm btn-primary " id="btnAddAction" href=""><i class="la flaticon-list"></i> Student</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form class="needs-validation" novalidate>
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <label for="validationTooltip01">First name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="First name" value="Mark" required>
                                                <div class="valid-tooltip">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="validationTooltip02">Last name</label>
                                                <input type="text" class="form-control" id="validationTooltip02" placeholder="Last name" value="Otto" required>
                                                <div class="valid-tooltip">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="validationTooltipUsername">Username</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="validationTooltipUsername" placeholder="Username" aria-describedby="validationTooltipUsernamePrepend" required>
                                                    <div class="invalid-tooltip">
                                                        Please choose a unique and valid username.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationTooltip03">City</label>
                                                <input type="text" class="form-control" id="validationTooltip03" placeholder="City" required>
                                                <div class="invalid-tooltip">
                                                    Please provide a valid city.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationTooltip04">State</label>
                                                <input type="text" class="form-control" id="validationTooltip04" placeholder="State" required>
                                                <div class="invalid-tooltip">
                                                    Please provide a valid state.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="validationTooltip05">Zip</label>
                                                <input type="text" class="form-control" id="validationTooltip05" placeholder="Zip" required>
                                                <div class="invalid-tooltip">
                                                    Please provide a valid zip.
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Submit form</button>
                                    </form>
                                    <form>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <legend class="col-form-label col-sm-2 pt-0">Radios</legend>
                                                <div class="col-sm-10">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                                        <label class="form-check-label" for="gridRadios1">
                                                            First radio
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                                        <label class="form-check-label" for="gridRadios2">
                                                            Second radio
                                                        </label>
                                                    </div>
                                                    <div class="form-check disabled">
                                                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
                                                        <label class="form-check-label" for="gridRadios3">
                                                            Third disabled radio
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                    <label class="form-check-label" for="inlineRadio1">1</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">2</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
                                                    <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <div class="checkbox checkbox-success">
                                                        <input id="checkbox_34" type="checkbox">
                                                        <label for="checkbox_34">Check me out !</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-2">Checkbox</div>
                                            <div class="col-sm-10">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="gridCheck1">
                                                    <label class="form-check-label" for="gridCheck1">
                                                        Example checkbox
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">&nbsp;</label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary">Sign in</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <form>
                                <div class="form-group row">
                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control form-control-sm" id="colFormLabelSm" placeholder="col-form-label-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabel" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="colFormLabel" placeholder="col-form-label">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select custom-select-sm">
                                            <option selected>Open this select menu</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select custom-select-lg mb-3">
                                            <option selected>Open this select menu</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <form name="frmAdd" method="post" action="" id="frmAdd" onsubmit="return validate();">
                                    <div id="mail-status"></div>
                                    <div>
                                        <label style="padding-top: 20px;">Name</label> <span id="name-info" class="info"></span><br> <input type="text" name="name" id="name" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Roll Number</label> <span id="roll-number-info" class="info"></span><br> <input type="text" name="roll_number" id="roll_number" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Date of Birth</label> <span id="dob-info" class="info"></span><br>
                                        <input type="date" name="dob" id="dob" class="demoInputBox">
                                    </div>
                                    <div>
                                        <label>Class</label> <span id="class-info" class="info"></span><br>
                                        <input type="text" name="class" id="class" class="demoInputBox">
                                    </div>
                                    <div>
                                        <input type="submit" name="add" id="btnSubmit" value="Add">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Base Form Control</div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="email2">Email Address</label>
                                <input type="email" class="form-control" id="email2" placeholder="Enter Email">
                                <small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Inline Input</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control input-full" id="inlineinput" placeholder="Enter Input">
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label for="successInput">Success Input</label>
                                <input type="text" id="successInput" value="Success" class="form-control">
                            </div>
                            <div class="form-group has-error has-feedback">
                                <label for="errorInput">Error Input</label>
                                <input type="text" id="errorInput" value="Error" class="form-control">
                                <small id="emailHelp" class="form-text text-muted">Please provide a valid informations.</small>
                            </div>
                            <div class="form-group">
                                <label for="disableinput">Disable Input</label>
                                <input type="text" class="form-control" id="disableinput" placeholder="Enter Input" disabled="">
                            </div>
                            <div class="form-check">
                                <label>Gender</label><br>
                                <label class="form-radio-label">
                                    <input class="form-radio-input" type="radio" name="optionsRadios" value="" checked="">
                                    <span class="form-radio-sign">Male</span>
                                </label>
                                <label class="form-radio-label ml-3">
                                    <input class="form-radio-input" type="radio" name="optionsRadios" value="">
                                    <span class="form-radio-sign">Female</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Static
                                </label>
                                <p class="form-control-static">hello@example.com</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Example select</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Example multiple select</label>
                                <select multiple="" class="form-control" id="exampleFormControlSelect2">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Example file input</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea class="form-control" id="comment" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 151px;">
										</textarea>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="">
                                    <span class="form-check-sign">Agree with terms and conditions</span>
                                </label>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Input Group</div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">@example.com</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="basic-url">Your vanity URL</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon3">https://example.com/users/</span>
                                    </div>
                                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">With textarea</span>
                                    </div>
                                    <textarea class="form-control" aria-label="With textarea"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-default btn-border" type="button">Button</button>
                                    </div>
                                    <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" aria-label="Text input with dropdown button">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-border dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</button>
                                        <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(384px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            <div role="separator" class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Form Group Styles</div>
                        </div>
                        <div class="card-body">
                            <label class="mb-3"><b>Form Group Default</b></label>
                            <div class="form-group form-group-default">
                                <label>Input</label>
                                <input id="Name" type="text" class="form-control" placeholder="Fill Name">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Select</label>
                                <select class="form-control" id="formGroupDefaultSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <label class="mt-3 mb-3"><b>Form Floating Label</b></label>
                            <div class="form-group form-floating-label">
                                <input id="inputFloatingLabel" type="text" class="form-control input-border-bottom" required="">
                                <label for="inputFloatingLabel" class="placeholder">Input</label>
                            </div>
                            <div class="form-group form-floating-label">
                                <select class="form-control input-border-bottom" id="selectFloatingLabel" required="">
                                    <option value="">&nbsp;</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <label for="selectFloatingLabel" class="placeholder">Select</label>
                            </div>
                            <div class="form-group form-floating-label">
                                <input id="inputFloatingLabel2" type="text" class="form-control input-solid" required="">
                                <label for="inputFloatingLabel2" class="placeholder">Input</label>
                            </div>
                            <div class="form-group form-floating-label">
                                <select class="form-control input-solid" id="selectFloatingLabel2" required="">
                                    <option value="">&nbsp;</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <label for="selectFloatingLabel2" class="placeholder">Select</label>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Form Control Styles</div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="squareInput">Square Input</label>
                                <input type="text" class="form-control input-square" id="squareInput" placeholder="Square Input">
                            </div>
                            <div class="form-group">
                                <label for="squareSelect">Square Select</label>
                                <select class="form-control input-square" id="squareSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pillInput">Pill Input</label>
                                <input type="text" class="form-control input-pill" id="pillInput" placeholder="Pill Input">
                            </div>
                            <div class="form-group">
                                <label for="pillSelect">Pill Select</label>
                                <select class="form-control input-pill" id="pillSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="solidInput">Solid Input</label>
                                <input type="text" class="form-control input-solid" id="solidInput" placeholder="Solid Input">
                            </div>
                            <div class="form-group">
                                <label for="solidSelect">Solid Select</label>
                                <select class="form-control input-solid" id="solidSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Form Control Styles</div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="largeInput">Large Input</label>
                                <input type="text" class="form-control form-control-lg" id="largeInput" placeholder="Large Input">
                            </div>
                            <div class="form-group">
                                <label for="largeInput">Default Input</label>
                                <input type="text" class="form-control form-control" id="defaultInput" placeholder="Default Input">
                            </div>
                            <div class="form-group">
                                <label for="smallInput">Small Input</label>
                                <input type="text" class="form-control form-control-sm" id="smallInput" placeholder="Small Input">
                            </div>
                            <div class="form-group">
                                <label for="largeSelect">Large Select</label>
                                <select class="form-control form-control-lg" id="largeSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="defaultSelect">Default Select</label>
                                <select class="form-control form-control" id="defaultSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="smallSelect">Small Select</label>
                                <select class="form-control form-control-sm" id="smallSelect">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Base Form Control</div>
                        </div>
                        <div class="card-body">

                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Name</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control form-control-sm input-full" id="inlineinput" placeholder="Enter Input">
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="inlineinput" class="col-md-3 col-form-label">Inline Input</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control form-control-sm input-full" id="inlineinput" placeholder="Enter Input">
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn btn-success">Submit</button>
                            <button class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<?php require_once ("layout/footer.php"); ?>
<!-- End Footer -->
